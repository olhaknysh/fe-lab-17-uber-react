import React, { lazy, Suspense, useEffect } from 'react';
import {  Switch } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { getCurrentUser } from './redux/auth/auth-operations';



import PrivateRoute from './components/PrivateRoute';
import PublicRoute from './components/PublicRoute';
import Loader from './components/Loader';

import routes from './utils/routes';


const HomePage = lazy(() =>
  import('./pages/HomePage' /* webpackChunkName: "HomePage */)
);

const LoginPage = lazy(() =>
  import('./pages/LoginPage' /* webpackChunkName: "LoginPage */)
);

const RegisterPage = lazy(() =>
  import('./pages/RegisterPage' /* webpackChunkName: "RegisterPage */)
);


const App = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getCurrentUser());
  }, []);

  return (
    <div className='container'>
      <Suspense fallback={<Loader />}>
        <Switch>
          <PublicRoute
            restricted
            path={routes.register}
            redirectTo={routes.home}
          >
            <RegisterPage />
          </PublicRoute>
          <PublicRoute
            restricted
            redirectTo={routes.home}
            path={routes.login}
          >
            <LoginPage />
          </PublicRoute>
          <PrivateRoute
            restricted
            redirectTo={routes.login}
            path={routes.home}
          >
            <HomePage />
          </PrivateRoute>
        </Switch>
      </Suspense>
    </div>
  );
};

export default App;
