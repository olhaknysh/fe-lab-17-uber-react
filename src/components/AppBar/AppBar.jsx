import React,{useEffect,useState} from 'react';
import styles from './AppBar.module.scss';
import { useDispatch, useSelector } from 'react-redux';
import { logout} from '../../redux/auth/auth-operations';
import {
  getUserEmail,
    getUserPhoto,
  isLoading
} from '../../redux/auth/auth-selectors';
import axios from 'axios';

import { BiExit } from 'react-icons/bi';
import Loader from '../../components/Loader'



const AppBar = () => {
    const [weather,setWeather] = useState({})
    useEffect(async() => {
    const {data} = await axios.get('/weather')
    console.log(data)
    setWeather(data)
    },[])
 
const dispatch = useDispatch();

const email = useSelector(getUserEmail);
const photo = useSelector(getUserPhoto);
const loading = useSelector(isLoading)



const handleLogout = () => dispatch(logout());

return (
<header className={styles.container}>
    <div className={styles.container}>
    {loading && <Loader />}
      <div className={styles.userInfo}>
        <div>
        <img className={styles.image} width='100' src={photo} alt='logo' />
        </div>
        <div>
            <h2>Weather</h2>
<p>{weather.description}, {weather.clouds} % clouds  in the sky</p>
<p>Temperature: {weather.temp}</p>
<p>Wind: {weather.wind_cdir_full}, {weather.wind_spd} m/s</p>
<p>Feels like {weather.feelsLikeTemp} degrees</p>
        </div>
        <div>
        <p>Welcome, {email}!</p>
        <button
          className={styles.button}
          onClick={handleLogout}
          type='button'
        >
          <BiExit /> Logout
        </button>
        </div>
     </div>
    </div>
</header>
);
}

export default AppBar;

