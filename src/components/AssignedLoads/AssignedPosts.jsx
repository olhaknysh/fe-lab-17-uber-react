import {useEffect,useState} from 'react'
import axios from 'axios';
import 'react-toastify/dist/ReactToastify.css';
import styles from './AssignedPosts.module.scss'

const AssignedLoads = () =>{
    const [loads,setLoads] = useState([])
    const [showModal, setShowModal] = useState(false);

    useEffect(async() => {
       const {data:loads} =  await axios.get('/loads?status=POSTED')

const  {loads:result} = loads
       setLoads(result)
    },[])


    const toggleModal = () => {
        setShowModal(!showModal);
      };
    

    return (
        <div>
        {loads.length === 0 ? <p>You have no posted loads</p> :
        <table className={styles.table}>
        <thead>
            <tr>
                <th>Load name</th>
                <th>Payload</th>
                <th>Pick-up address</th>
                <th>Delivery address</th>
            </tr>
        </thead>
        <tbody>
            {loads.length >0 && loads.map((load) =>(
                <tr key={load._id}>
                    <td>{load.name}</td>
                    <td>{load.payload}</td>
                    <td>{load.pickup_address}</td>
                    <td>{load.delivery_address}</td>
                </tr>
            ))}
        </tbody>
    </table>
        }
        </div>
    )

}

export default AssignedLoads;