import {useEffect,useState} from 'react'
import axios from 'axios';
import 'react-toastify/dist/ReactToastify.css';

const History = () =>{
    const [loads,setLoads] = useState([])
    const [showModal, setShowModal] = useState(false);

    useEffect(async() => {
       const {data:loads} =  await axios.get('/loads')

const  {loads:result} = loads
console.log(result)
       setLoads(result)
    },[])


    

    return (
       <ul>
           {loads.length >0 &&
           loads.map((load) => (
               <li>
                   <p>{load.name}</p>
                   {load.logs.map((log) => (
                       <p>{log.message}</p>
                   ))}
               </li>
           ))}
        </ul>
    )

}

export default History;