import React,{useState} from 'react';
import NewLoads from '../NewLoads'
import AssignedLoads from '../AssignedLoads'
import PostedLoads from '../PostedLoads'
import History from '../History'
import Profile from '../Profile'


import routes from '../../utils/routes';
import styles from './Navigation.module.scss';


const LoadNavigation = () => {
   const [newLoads,setNewloads] = useState(true)
   const [postedLoads,setPostedloads] = useState(false)
   const [assignedLoads,setAssgnedloads] = useState(false)
   const [history,setHistory] = useState(false)
   const [profile,setProfile] = useState(false)

   const handleNew = () =>{
    setNewloads(true)
    setPostedloads(false)
    setAssgnedloads(false)
    setHistory(false)
    setProfile(false)
   }

   const handlePost = () =>{
    setNewloads(false)
    setPostedloads(true)
    setAssgnedloads(false)
    setHistory(false)
    setProfile(false)
   }

   const handleAssign = () =>{
    setNewloads(false)
    setPostedloads(false)
    setAssgnedloads(true)
    setHistory(false)
    setProfile(false)  
   }

   const handleHistory = () =>{
    setNewloads(false)
    setPostedloads(false)
    setAssgnedloads(false)
    setHistory(true)
    setProfile(false)   
   }

   const handleProfile = () =>{
    setNewloads(false)
    setPostedloads(false)
    setAssgnedloads(false)
    setHistory(false)
    setProfile(true)   
   }

    return (
        <div className={styles.container}>
            <div >
            <button className={styles.button} onClick={handleNew} type="button">New loads</button>
        <button className={styles.button} onClick={handlePost} type="button">Assigned loads</button>
        <button className={styles.button} onClick={handleAssign} type="button">Posted loads</button>
        <button className={styles.button} onClick={handleHistory} type="button">History</button>
        <button className={styles.button} onClick={handleProfile} type="button">Profile</button>
            </div>
            <div>
            {newLoads && <NewLoads />}
        {postedLoads && <PostedLoads />}
        {assignedLoads && <AssignedLoads />}
        {history && <History />}
        {profile && <Profile />}
            </div>
      </div>
    )
}


export default LoadNavigation;
