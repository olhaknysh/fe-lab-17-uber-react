import {useEffect,useState} from 'react'
import axios from 'axios';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Modal from '../Modal';
import styles from './NewLoads.module.scss'

const NewLoads = () =>{
    const [loads,setLoads] = useState([])
    const [showModal, setShowModal] = useState(false);

    const [name,setName] = useState('')
    const [payload,setPayload] = useState(0)
    const [pickup_address,setPickup_address] = useState('')
    const [delivery_address,setDelivery_address] = useState('')
    const [width,setWidth] = useState(0)
    const [length,setLength] = useState(0)
    const [height,setHeight] = useState(0)

    useEffect(async() => {
       const {data:loads} =  await axios.get('/loads?status=NEW')

const  {loads:result} = loads
       setLoads(result)
    },[])

    const handleName = ({target}) => setName(target.value)
    const handlePayload = ({target}) => setPayload(target.value)
    const handlePickup = ({target}) => setPickup_address(target.value)
    const handleDelivary = ({target}) => setDelivery_address(target.value)
    const handleWidth = ({target}) => setWidth(target.value)
    const handleLength = ({target}) => setLength(target.value)
    const handleHeigth = ({target}) => setHeight(target.value)

    const toggleModal = () => {
        setShowModal(!showModal);
      };

      const handlePost = async(e) =>{
         const {id} = e.target.dataset
         await axios.post(`/loads/${id}/post`)
         const {data:loads} =  await axios.get('/loads?status=NEW')
         const  {loads:result} = loads
         setLoads(result)

         toast.configure()
         toast.info('You have posted the load')
      }

      const handleSubmit = async(e) =>{
          e.preventDefault()
          try{
               await axios.post('/loads', {
                name,
                payload:Number(payload),
                pickup_address,
                delivery_address,
                dimensions:{
                    width:Number(width),
                    length:Number(length),
                    height:Number(height)
                }})

                toggleModal()
                const {data:loads} =  await axios.get('/loads?status=NEW')
                const  {loads:result} = loads
                setLoads(result)
          }catch(error){
              toast.configure()
              toast.error(error.message)
          }
      }
    

    return (
        <div>
        {loads.length === 0 ? <p>You have no new loads</p> :
        <table className={styles.table}>
        <thead>
            <tr>
                <th>Load name</th>
                <th>Payload</th>
                <th>Pick-up address</th>
                <th>Delivery address</th>
            </tr>
        </thead>
        <tbody>
            {loads.length >0 && loads.map((load) =>(
                <tr key={load._id}>
                    <td>{load.name}</td>
                    <td>{load.payload}</td>
                    <td>{load.pickup_address}</td>
                    <td>{load.delivery_address}</td>
                    <td>
                        <button onClick={handlePost} data-id={load._id} type="button">Post</button>
                    </td>
                </tr>
            ))}
        </tbody>
    </table>
        }
        <button className={styles.button} onClick={toggleModal} type="button">Add new load</button>
           {showModal && (
          <Modal onClose={toggleModal}>
            <form onSubmit={handleSubmit} className={styles.form}>
                <label>Name
                <input onChange={handleName} value={name} type="text"/>
                </label>
                <label>Payload
                <input onChange={handlePayload} value={payload} type="text"/>
                </label>
                <label>Pickup address
                <input onChange={handlePickup} value={pickup_address} type="text"/>
                </label>
                <label>Delivery address
                <input onChange={handleDelivary} value={delivery_address} type="text"/>
                </label>
                <label>Width
                <input onChange={handleWidth} value={width} type="text"/>
                </label>
                <label>Heigth
                <input onChange={handleHeigth} value={height} type="text"/>
                </label>
                <label>Length
                <input onChange={handleLength} value={length} type="text"/>
                </label>
                <button className={styles.button} type='submit'>Add new load</button>
            </form>
          </Modal>
        )}
        </div>
    )

}

export default NewLoads;