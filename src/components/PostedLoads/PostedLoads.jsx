import {useEffect,useState} from 'react'
import axios from 'axios';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Modal from '../Modal';
import styles from './PostedLoads.module.scss'

const PostedLoads = () =>{
    const [loads,setLoads] = useState([])
    const [showModal, setShowModal] = useState(false);

    useEffect(async() => {
       const {data:loads} =  await axios.get('/loads?status=ASSIGNED')

const  {loads:result} = loads
       setLoads(result)
    },[])


    const toggleModal = () => {
        setShowModal(!showModal);
      };
    

    return (
        <div>
        {loads.length === 0 ? <p>You have no assigned loads</p> :
        <table className={styles.table}>
        <thead>
            <tr>
                <th>Load name</th>
                <th>Payload</th>
                <th>Pick-up address</th>
                <th>Delivery address</th>
                <th>Assign to</th>
            </tr>
        </thead>
        <tbody>
            {loads.length >0 && loads.map((load) =>(
                <tr key={load._id}>
                    <td>{load.name}</td>
                    <td>{load.payload}</td>
                    <td>{load.pickup_address}</td>
                    <td>{load.delivery_address}</td>
                    <td>{load.assigned_to}</td>
                </tr>
            ))}
        </tbody>
    </table>
        }
        </div>
    )

}

export default PostedLoads;