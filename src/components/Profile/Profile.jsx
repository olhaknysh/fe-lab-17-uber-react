import {useEffect,useState} from 'react'
import axios from 'axios';
import 'react-toastify/dist/ReactToastify.css';
import { logout,changeAvatar } from '../../redux/auth/auth-operations';
import { useDispatch, useSelector } from 'react-redux';
import styles from './Profile.module.scss'

const Profile = () =>{
    const dispatch = useDispatch();
    const [profile,setProfile] = useState({})

    useEffect(async() => {
       const {data:user} =  await axios.get('/users/me')
       const{user:me} = user
       console.log(me)
       setProfile(me)
    },[])

    const uploadHandler =(event) =>{
        dispatch(changeAvatar(event.target.files[0]))
        }


    

    return (
   <div className={styles.container}>
    <p>Hello, {profile.email}!</p>
    <p>Your role is {profile.role}</p>

    <label>Change avatar  <input type="file" name="file" onChange={uploadHandler}/></label>
     
   </div>
    )

}

export default Profile;