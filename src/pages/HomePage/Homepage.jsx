import Appbar from '../../components/AppBar'
import LoadNavigation from '../../components/LoadNavigation'
import {useSelector} from 'react-redux';
import {getUserRole,getIsAuthenticated} from '../../redux/auth/auth-selectors'

const Homepage = () =>{
    const role =useSelector(getUserRole)
    const isAuthenticated = useSelector(getIsAuthenticated)
return (
    <>
        {isAuthenticated && <Appbar/>}
        {role === 'SHIPPER' && <LoadNavigation/> }
    </>
)
}

export default Homepage