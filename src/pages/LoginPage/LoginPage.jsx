import { useState,useRef,useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import {  login,forgotpassword ,getCurrentUser} from '../../redux/auth/auth-operations';
import { isLoading } from '../../redux/auth/auth-selectors';

import Loader from '../../components/Loader';

import routes from '../../utils/routes';
import styles from './LoginPage.module.scss';

import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


const initialValue = {
  email: '',
  password: '',
};


const LoginPage = () => {
  const dispatch = useDispatch();
  const inputRef = useRef(null);
  const [state, setState] = useState(initialValue);
  const { email, password } = state;

  useEffect(() => {
      inputRef.current.focus();
    }, []);

   
  const loading = useSelector(isLoading);
    

  const handleInputChange = (e) => {
    setState((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }));
  };

  const handleForgotPassword = () =>{
      if(email){
        dispatch(forgotpassword(email))
      }else{
      toast.configure();
      toast.info('Please enter your email')
      }
  }

  const handleSubmit = async(e) => {
    e.preventDefault();
    const user = {
      email,
      password,
    };
    await dispatch(login(user));
    await dispatch(getCurrentUser())
    setState(initialValue);
  };

  return (
    <div className={styles.container}>
      {loading && <Loader />}
        <h2>Login</h2>
    
      <form onSubmit={handleSubmit} autoComplete='off'>
        <div className={styles.field}>
          <label htmlFor='email'>Email</label>
          <input
            value={email}
            type='text'
            id='email'
            name='email'
            placeholder='Enter you email'
            onChange={handleInputChange}
            className={styles.input}
            ref={inputRef}
          />
        </div>
        <div className={styles.field}>
          <label htmlFor='password'>Password</label>
          <input
            value={password}
            type='text'
            id='password'
            name='password'
            placeholder='Enter you password'
            onChange={handleInputChange}
            className={styles.input}
          />
        </div>
        <button className={styles.button} type='submit'>
          Log in
        </button>
        <button onClick={handleForgotPassword} className={styles.button} type='button'>
          Forgot password
        </button>
      </form>

      <p className={styles.text}>
        Don’t have an account?
        <Link className={styles.link} to={routes.register}>
         Register
        </Link>
      </p>
    </div>
  );
};

export default LoginPage;
