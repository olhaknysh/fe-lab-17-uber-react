import { useState, useRef, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import {register,login,getCurrentUser } from '../../redux/auth/auth-operations';
import { isLoading } from '../../redux/auth/auth-selectors';

import Loader from '../../components/Loader';


import routes from '../../utils/routes';
import styles from './RegisterPage.module.scss';


const initialValue = {
  email: '',
  password: '',
  role: '',
};


const RegisterPage = () => {
  const dispatch = useDispatch();
  const inputRef = useRef(null);
  const [state, setState] = useState(initialValue);
  const { email, password, role } = state;
    
   useEffect(() => {
       inputRef.current.focus();
     }, []);

  const loading = useSelector(isLoading);
    

  const handleInputChange = (e) => {
    setState((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }));
  };

  const handleUserRegister = async (user) =>{
    await dispatch(register(user));
    const {email,password} = user 
    await dispatch(login({email,password}))
    await dispatch(getCurrentUser())
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    const user = {
      email,
      password,
      role,
    };
    handleUserRegister(user)
    setState(initialValue);
  };

  return (
    <div className={styles.container}>
      {loading && <Loader />}
        <h2>Register</h2>
      <form onSubmit={handleSubmit} autoComplete='off'>
        <div className={styles.field}>
          <label htmlFor='userName'>Role</label>
          <input
            value={role}
            type='text'
            id='role'
            name='role'
            placeholder='Enter you role(SHIPPER or DRIVER)'
            onChange={handleInputChange}
            className={styles.input}
            ref={inputRef}
          />
        </div>
        <div className={styles.field}>
          <label htmlFor='email'>Email</label>
          <input
            value={email}
            type='text'
            id='email'
            name='email'
            placeholder='Enter you email'
            onChange={handleInputChange}
            className={styles.input}
          />
        </div>
        <div className={styles.field}>
          <label htmlFor='password'>Password</label>
          <input
            value={password}
            type='text'
            id='password'
            name='password'
            placeholder='Enter you password'
            onChange={handleInputChange}
            className={styles.input}
          />
        </div>
        <button className={styles.button} type='submit'>
          Create Account
        </button>
      </form>

      <p>
        Already have an account?
        <Link className={styles.link} to={routes.login}>
          Login
        </Link>
      </p>
    </div>
  );
};

export default RegisterPage;
