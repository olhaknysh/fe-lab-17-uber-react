import axios from 'axios';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import authActions from './auth-actions';

axios.defaults.baseURL = 'https://fierce-beach-10656.herokuapp.com/api'
const setToken = token => {
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
  };

export const login =
  ({ email, password }) =>
  async (dispatch) => {
    dispatch(authActions.loginRequest());

    try {
    const {data} = await axios.post('auth/login',{email,password})
    setToken(data.jwt_token)
    dispatch(authActions.loginSuccess(data.jwt_token));

    } catch (error) {
      toast.configure();
      toast.error(`Error during login:${error.message}`);
      dispatch(authActions.loginError(error.message));
    }
  };

export const register =
  ({ email, password, role }) =>
  async (dispatch) => {
    dispatch(authActions.registerRequest());

    try {
    await axios.post('auth/register',{email,password,role})
    dispatch(authActions.registerSuccess());
    
    } catch (error) {
      toast.configure();
      toast.error(`Error during registration:${error.message}`);
      dispatch(authActions.registerError(error.message));
    }
  };

export const logout = () => async (dispatch) => {
  dispatch(authActions.logoutRequest());

  try {
    await axios.post('auth/logout')
    dispatch(authActions.logoutSuccess());
  } catch (error) {
    toast.configure();
    toast.error(`Error during logout:${error.message}`);
    dispatch(authActions.logoutError(error.message));
  }
};

export const getCurrentUser = () => async (dispatch, getState) => {
  const state = getState();
  const { token } = state.auth;
  setToken(token);

  if (!token) {
    return;
  }

  dispatch(authActions.getCurrentUserRequest());
  try {
  const {data:{user}} = await axios.get('users/me')
   dispatch(authActions.getCurrentUserSuccess(user))

  } catch (error) {
    toast.configure();
    toast.error(`Error during getting current user:${error.message}`);
    dispatch(authActions.getCurrentUserError(error.message));
  }
};

export const forgotpassword = (email) => async(dispatch) =>{
    try {
         const {data} = await axios.post('auth/forgot_password',{email})
         toast.info(data.message);
      
        } catch (error) {
          toast.configure();
          toast.error(`Error during getting password:${error.message}`);
          dispatch(authActions.getCurrentUserError(error.message));
        }
}

export const changeAvatar = (file) => async(dispatch) =>{
    dispatch(authActions.updateAvatarUserRequest())


    try {
        const {data} = await axios.patch('auth/avatars',{file})
        console.log(data)
     
       } catch (error) {
         toast.configure();
         toast.error(`Error during getting password:${error.message}`);
         dispatch(authActions.updateAvatarUserError(error.message));
       }
}

