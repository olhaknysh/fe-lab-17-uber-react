import { combineReducers } from 'redux';
import { createReducer } from '@reduxjs/toolkit';
import authActions from './auth-actions';

const email = createReducer('', {
  [authActions.getCurrentUserSuccess]: (_, { payload }) => payload.email,
  [authActions.logoutSuccess]: () => '',
});

const token = createReducer('', {
  [authActions.loginSuccess]: (_, { payload }) => payload,
  [authActions.logoutSuccess]: () => '',
});


const photoURL = createReducer('', {
  [authActions.getCurrentUserSuccess]: (_, { payload }) => payload.avatarURL,
  [authActions.logoutSuccess]: () => '',
});

const role = createReducer('', {
    [authActions.getCurrentUserSuccess]: (_, { payload }) => payload.role,
    [authActions.logoutSuccess]: () => '',
  });


const setError = (_, { payload }) => payload;
const error = createReducer(null, {
  [authActions.registerError]: setError,
  [authActions.loginError]: setError,
  [authActions.logoutError]: setError,
  [authActions.getCurrentUserError]: setError,
  [authActions.registerSuccess]: () => null,
  [authActions.loginSuccess]: () => null,
  [authActions.logoutSuccess]: () => null,
  [authActions.getCurrentUserSuccess]: () => null,

});

const isAuthenticated = createReducer(false, {
  [authActions.loginSuccess]: () => true,
  [authActions.getCurrentUserSuccess]: () => true,
  [authActions.registerError]: () => false,
  [authActions.loginError]: () => false,
  [authActions.getCurrentUserError]: () => false,
  [authActions.logoutSuccess]: () => false,
});

const isLoading = createReducer(false, {
  [authActions.getCurrentUserRequest]: () => true,
  [authActions.loginRequest]: () => true,
  [authActions.logoutRequest]: () => true,
  [authActions.registerRequest]: () => true,
  [authActions.getCurrentUserSuccess]: () => false,
  [authActions.getCurrentUserError]: () => false,
  [authActions.loginSuccess]: () => false,
  [authActions.loginError]: () => false,
  [authActions.logoutSuccess]: () => false,
  [authActions.logoutError]: () => false,
  [authActions.registerSuccess]: () => false,
  [authActions.registerError]: () => false,
});

export default combineReducers({
  email,
  error,
  isAuthenticated,
  isLoading,
  token,
  photoURL,
  role
});
