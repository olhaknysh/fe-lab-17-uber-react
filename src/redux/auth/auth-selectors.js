export const getIsAuthenticated = (state) => state.auth.isAuthenticated;

export const getUserEmail = (state) => state.auth.email;
export const getUserPhoto = (state) => state.auth.photoURL;
export const getUserRole = state => state.auth.role

export const isLoading = (state) => state.auth.isLoading;
export const error = (state) => state.auth.error;
